# 首次发布
	1、UI版本基于批处理版本，功能大致相同
	2、版本提供基本的站点搭建服务，Apache服务名为：Apache_Mrdoc

# 使用简介

##  1、首次使用初始化：
### 需要进行环境初始化，初始化内容主要包括：
	自动安装Git
	下载python环境
	下载Apache环境
	
	

![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/210233_f16dc873_1181475.png "屏幕截图.png")


## 2、部署Mrdoc源码
	本步骤主要用于下载mrdoc源码文件，源：Gitee仓库 （其中仓库地址可更改）
### 2.1 设置代码仓库地址
#### 默认为Mrdoc开源版本地址、下图中红色框内为Git仓库地址，可自行更改，此地址仅用于Clone功能
#### 蓝色框为Django项目名称，点击按钮会根据地址自动获取项目名，项目变更后一定要点击按钮
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/210244_476a7781_1181475.png "屏幕截图.png")

### 2.2 克隆代码
#### 下图中蓝色框，点击后直接Clone地址中的文件

![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/210254_8d87f016_1181475.png "屏幕截图.png")


### 2.3 依赖安装及初始化
#### 初始化数据迁移，本步骤用于环境依赖安装及数据库变更

![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/210309_5b56a9c4_1181475.png "屏幕截图.png")

### 2.4至此，网站环境已经配置完成（管理员帐户为创建，执行【3】可以创建帐户）
` Django的管理员创建必须在cmd模式下进行，请注意甄别 `
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/210314_0562140b_1181475.png "屏幕截图.png")


## 3、网站启动

### 3.1 Runserver启动
`此模式启动是会弹出命令行窗口，方便调试`
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/210323_53feaae2_1181475.png "屏幕截图.png")


### 3.2 Apache模式部署
`此模式会安装为系统服务，会随系统开机启动，用于部署windows环境`


![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/210327_f7fcc091_1181475.png "屏幕截图.png")

## 4、多服务管理
### 对服务右键可以进行启动、停止、重启、删除等操作

![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/210332_8d43a820_1181475.png "屏幕截图.png")